package enam;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum Role {

    DRIVER("DRIVER"),
    ADMIN("ADMIN"),
    COMPANY("COMPANY");

    private final String role;
}
