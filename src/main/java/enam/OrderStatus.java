package enam;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@Getter
@RequiredArgsConstructor
public enum OrderStatus {
    FINISHED("FINISHED"), PROGRESS("PROGRESS"), EXPECTATION("EXPECTATION"), INVALID("INVALID");

    private final String status;
}
