package db.entity;


import com.fasterxml.jackson.annotation.JsonProperty;
import enam.OrderStatus;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;

@Entity
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "orders", schema = "public")
public class Order {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gen")
    @SequenceGenerator(name = "gen", sequenceName = "orders_id_seq", allocationSize = 1, schema = "public")
    @Column(name = "id")
    private Long id;

    @NotBlank(message = "The description must not consist of an empty string or be null")
    @NotEmpty(message = "The description should not be null")
    @Column(name = "description")
    private String description;

    @JsonProperty(value = "time_create_order")
    @Column(name = "time_create_order")
    private String timeCreateOrder;

    @Column(name = "time_completion_order")
    private String timeCompletionOrder;

    @Column(name = "status")
    @Enumerated(value = EnumType.STRING)
    private OrderStatus status;

    @JsonProperty(value = "address_to")
    @NotEmpty(message = "The address cannot be null")
    @NotBlank(message = "An address cannot contain an empty string containing only spaces")
    @Column(name = "address_to")
    private String addressTo;

    @JsonProperty(value = "address_from")
    @NotEmpty(message = "The address cannot be null")
    @NotBlank(message = "An address cannot contain an empty string and containing only spaces")
    @Column(name = "address_from")
    private String addressFrom;

    @JoinColumn(name = "department_id")
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Department department;

    @NotEmpty(message = "The phone cannot be null")
    @NotBlank(message = "An phone cannot contain an empty string and containing only spaces")
    @Column(name = "phone")
    private String phone;

    @JsonProperty(value = "name_client")
    @NotEmpty(message = "The name cannot be null")
    @NotBlank(message = "The name cannot contain only empty spaces")
    @Column(name = "name_client")
    private String nameClient;

    @JoinColumn(name = "driver_id")
    @ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
    private Driver driver;

}
