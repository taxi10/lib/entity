package db.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@Entity
@Table(name = "spring_session")
@AllArgsConstructor
@NoArgsConstructor
public class MySession {

    @Id
    @Column(name = "primary_id")
    private String id;

    @Column(name = "session_id")
    private String sessionId;

    @Column(name = "principal_name")
    private String principalName;
}
